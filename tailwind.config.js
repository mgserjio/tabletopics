/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,css,js,cljs}", "./resources/**/*/*.html"],
  theme: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: [
      {
        custom: {
          "primary": "#b39bdd",
          "secondary": "#893bef",
          "accent": "#92efc7",
          "neutral": "#2E222F",
          "base-100": "#34384C",
          "info": "#3D6ED1",
          "success": "#126457",
          "warning": "#A86F05",
          "error": "#E0384F",
        },
        custom2: {
          "primary": "#515ec1",
          "secondary": "#e2c312",
          "accent": "#88e209",
          "neutral": "#202137",
          "base-100": "#3D3545",
          "info": "#46A8D2",
          "success": "#1B985D",
          "warning": "#F7C04A",
          "error": "#F95D5D",
        },
      },
    ],
  },
};
