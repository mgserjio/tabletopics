(ns toastmasters
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]))

(defn make-stack [title & topics]
  {:title title
   :pool (vec topics)
   :unused (into #{} (-> topics count range))
   :used #{}})

(defn topics-count [stack]
  (-> stack :pool count))

(defn topics-left [stack]
  (-> stack :unused count))

(defn has-more? [stack]
  (not= (topics-left stack) 0))

(defn has-topic? [stack index]
  ((:unused stack) index))

(defn pull-topic [stack index]
  (when (has-topic? stack index)
    (get-in stack [:pool index])))

(defn use-topic [stack index]
  (if (has-topic? stack index)
    (-> stack
        (assoc-in [:unused] (-> stack :unused (disj index)))
        (assoc-in [:used] (-> stack :used (conj index))))))

(defn random-topic-index [stack]
  (-> stack :unused seq rand-nth))

(defn used-topics [stack]
  (:used stack))

(defn stack-title [stack]
  (:title stack))

(comment
  (let [stack (make-stack "First one", "lorem", "ipsum")]

    (println stack)

    (println (topics-count stack))

    (println (pull-topic stack 100))
    (println (pull-topic stack 1))
(println (use-topic stack 1))

    (println (random-topic-index stack))

    ))


(defonce state (r/atom
                {:stacks [(make-stack "Me"
                                      "My biggest fear"
                                      "My greatest desire"
                                      "My most precious thing"
                                      "Book that influenced my life the most"
                                      "Person that influenced my life the most"
                                      "The thing I should've done but didn't dare"
                                      "My contribution to the better world")

                          (make-stack "Start from Monday"
                                      "Is it hard to change a habit or get a new one?"
                                      "The most popular things to start from Monday"
                                      "How to actually start doing what you've planned?"
                                      "Habit I want to develop or get rid of"
                                      "What gets in the way to being a better self"
                                      "Accountability and support role in achieving goals and building habits"
                                      "One habit to focus on for the rest of the year. What would it be and why?"
                                      "One thing you keep starting from Monday")

                          (make-stack "Art"
                                      "Does Birth of AI means Death of Art?"
                                      "Can Math be called Art? [Skippable]"
                                      "Which form of Art is the Queen of all the others?"
                                      "The subtle Art to listen (of listening)"
                                      "How has technology influenced art in recent years?"
                                      "Is art a luxury or a necessity"
                                      "Art for art's sake or art for a cause?")

                          ;; (make-stack "Environment"
                          ;;             "Why we still can’t ban plastic and oil? Managed to force the lockdown, though."
                          ;;             "Toxic people. How do we save the planet?"
                          ;;             "What I can do to reduce the load on the environment")

                          (make-stack "Get off my loan!"
                                      "Why adults forget how to be kids"
                                      "How to get along with parents?"
                                      "Lessons learnt from parents that helped you in life"
                                      "How to express needs to parents/children without conflict?"
                                      "The importance of quality time with our parents (or kids)")]

                 :used []
                 :show-modal false}))

(defn use-topic-from-stack [stack-index]
  (let [stack (-> @state :stacks (nth stack-index))]
    (if (has-more? stack)
      (let [topic-index (random-topic-index stack)
            topic (pull-topic stack topic-index)
            stack' (use-topic stack topic-index)]
        (swap! state
               #(-> %1
                    (assoc-in [:stacks stack-index] stack')
                    (update-in [:used] conj topic))))
      (js/alert "No More topics in this stack!"))))


(defn card
  ([stack]
   (card {} stack))
  ([attrs stack]
   [:div.card.bg-secondary
    attrs
    [:div.card-body.justify-center.items-center
     [:h2.card-title.text-5xl.mb-2.cursor-pointer (:title stack)]
     [:div.card-actions.justify-center
      [:div.badge (topics-left stack)]
      "/"
      [:div.badge (topics-count stack)]]]]))

(defn topic-modal [{on-click :on-click} topic]
  [:<>
   [:input.modal-toggle {:type "checkbox" :id "topic-modal" :checked true}]
   [:div.modal>div.modal-box.py-16.px-8 {:class "w-9/12 max-w-3xl"}
    [:h1.font-bold.text-5xl topic]
    [:div.modal-action>button.btn.mt-12 {:on-click on-click} "Next!"]]])

(defn show-modal []
  (swap! state #(assoc-in %1 [:show-modal] true)))

(defn close-modal []
  (swap! state #(assoc-in %1 [:show-modal] false)))

(defn app []
  [:div.max-w-7xl.h-screen.mx-auto.flex.flex-col.justify-center
   (when (:show-modal @state)
     (topic-modal {:on-click close-modal}
                  (-> @state :used last)))
   [:div.flex.flex-col.gap-2 ;;{:class "basis-2/3"}
    (let [stacks (:stacks @state)]
      (for [[key stack] (map vector (range) stacks)]
        ^{:hey key}
        [card {:on-click #((do
                             (use-topic-from-stack key)
                             (show-modal)))} stack]))]])

(defn render []
  (rdom/render [app]
               (js/document.getElementById "app")))

(render)
